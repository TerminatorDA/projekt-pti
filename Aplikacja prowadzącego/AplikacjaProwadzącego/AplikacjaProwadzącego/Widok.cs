﻿using PTI_TCP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace AplikacjaProwadzącego
{
	class Widok
	{
		public ICollectionView widok { get; private set; }
		
        public Widok(Dictionary<int, Proces> listaProcesow)
		{	
			List<Proces> procesy = new List<Proces>();

			foreach (int klucz in listaProcesow.Keys)
			{
				procesy.Add(listaProcesow[klucz]);
			}

			procesy = procesy.OrderByDescending(x => x.zuzyciePamieci).ToList<Proces>();

            widok = CollectionViewSource.GetDefaultView(procesy);
        }
	}
}
