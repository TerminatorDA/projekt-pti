﻿using PTI_TCP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AplikacjaProwadzącego
{
	/// <summary>
	/// Interaction logic for BlackList.xaml
	/// </summary>
	public partial class BlackList : Window
	{
		public BlackList()
		{
			InitializeComponent();

			Console.WriteLine("Będę dodawał do list");
			
			try
			{
				Console.Write("Na początek z pliku, wczytuję... ");
				String[] known = File.ReadAllLines("known.txt");
				Console.WriteLine(known.Length + " linii");
				foreach(String z in known)
				{
					Console.WriteLine("Dodaję " + z + " do listy");
					znane.Items.Add(z);
				}
				Console.WriteLine("Teraz będę dodawał z podłączonych stanowisk...");
				Console.WriteLine("Znalazłem " + ((App)Application.Current).serwer.listaStanowisk.Count + " stanowisk");
				Console.WriteLine("Znalazłem " + ((App)Application.Current).serwer.listaStanowisk.First().Value.listaProcesow.Count + " procesów piewrszego stanowiska");
				foreach (var s in ((App)Application.Current).serwer.listaStanowisk)
				{
					foreach (var p in s.Value.listaProcesow)
					{
						Console.WriteLine("Jeśli jeszcze nie ma na liście to dodam " + p.Value.nazwa);
						if (znane.FindName(p.Value.nazwa) == null)
						{
							znane.Items.Add(p.Value.nazwa);
							Console.WriteLine("Dodałem " + p.Value.nazwa);
						}	
					}
				}
			}
			catch(IOException e)
			{
				Console.WriteLine("Problem z obsługą pliku " + e.Message);
			}
			catch(Exception e)
			{
				Console.WriteLine("Wystąpił błąd podczas dodawania elementów " + e.Message);
			}

			try
			{
				foreach (var v in ((App)Application.Current).blackList)
				{
					object o = black.FindName(v);

					if (o == null)
					{
						black.Items.Add(v);

						Console.WriteLine("Dodaję v " + v);
					}

					for (int i = 0; i < znane.Items.Count; i++)
					{
						if (znane.Items[i].ToString().Contains(v))
						{
							znane.Items.RemoveAt(i);
						}
					}
				}
			}
			catch(Exception e)
			{
				Console.WriteLine("Wystąpił bład " + e.Message);
			}
		}

		private void newBlackList(object sender, EventArgs args)
		{
			((App)Application.Current).blackList = black.Items.Cast<String>().ToList();

			StreamWriter file = new System.IO.StreamWriter("black.txt");

			try
			{
				foreach(var f in ((App)Application.Current).blackList)
				{
					file.WriteLine(f.ToString());
				}
			}
			catch(Exception e)
			{
				Console.WriteLine("Nie udało się zapisać blackListy do pliku " + e.Message);
			}
			finally
			{
				file.Close();
			}
		}

		private void dodajZaznaczone(object sender, RoutedEventArgs e)
		{
			foreach (var i in znane.SelectedItems)
			{
				black.Items.Add(i);
			}

			foreach(var c in black.Items)
			for (int i = 0; i < znane.Items.Count; i++)
			{
				if (znane.Items[i].ToString().Contains(c.ToString()))
				{
					znane.Items.RemoveAt(i);
				}
			}
		}

		private void usunZaznaczone(object sender, RoutedEventArgs e)
		{
			List<String> zaznaczone = new List<String>();
			foreach (var i in black.SelectedItems)
			{
				zaznaczone.Add(i.ToString());
				Console.WriteLine("Zaznaczone: " + i.ToString());
			}

			for (int j = black.Items.Count - 1; j >= 0; --j)
			{
				for (int i = 0; i < zaznaczone.Count; i++)
				{
					Console.WriteLine("Porównuję " + zaznaczone[i] + " z " + black.Items[j]);
					if (black.Items[j].ToString().Contains(zaznaczone[i]))
					{
						black.Items.RemoveAt(j);
						znane.Items.Add(zaznaczone[i]);
						break;
					}
				}
			}
		}

		private void dodajNazwe(object sender, RoutedEventArgs e)
		{
			if(nowy.Text != null && nowy.Text != "")
			{
				black.Items.Add(nowy.Text);
			}
		}

		private void usunNazwe(object sender, RoutedEventArgs e)
		{
			if(stary.Text != null && stary.Text != "")
			{
				for (int j = black.Items.Count - 1; j >= 0; --j)
				{
					if (black.Items[j].ToString().Contains(stary.Text))
					{
						black.Items.RemoveAt(j);
					}
				}
			}
		}
	}
}
