﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace PTI_TCP
{
	public class Stanowisko
	{
		/// <summary>Nazwa aktualnie zalogowanego użytkownika na stanowisku</summary>
		public string nazwaUzytkownika { get; private set; }

		/// <summary>Nazwa komputera</summary>
		public string nazwaKomputera { get; private set; }

		/// <summary>Słownik aktualnie działających procesów z PID jako klucz</summary>
		public Dictionary<int, Proces> listaProcesow { get; private set; }

		/// <summary>Aktualny zrzut ekranu stanowiska</summary>
		public Image aktualnyZrzutEkranu { get; set; }

		/// <summary>Utworzenie nowego stanowiska</summary>
		/// <param name="nazwaKomputera">Nazwa komputera</param>
		/// <param name="nazwaUzytkownika">Nazwa aktualnie zalogowanego użytkownika na stanowisku</param>
		public Stanowisko(string nazwaKomputera, string nazwaUzytkownika)
		{
			this.nazwaKomputera = nazwaKomputera;
			this.nazwaUzytkownika = nazwaUzytkownika;
			listaProcesow = new Dictionary<int, Proces>();
		}

		/// <summary>Dodaje lub, jeśli istnieje, edytuje proces o podanym PID</summary>
		/// <param name="PID">ID procesu, jednoznacznie go identyfikujący</param>
		/// <param name="nowyProces">Obiekt procesu</param>
		public void dodajProces(int PID, Proces nowyProces)
		{
			listaProcesow[PID] = nowyProces;
		}

		/// <summary>Usuwa z listy proces o podanym PID</summary>
		/// <param name="PID">ID procesu, jednoznacznie go identyfikujący</param>
		public void usunProces(int PID)
		{
			listaProcesow.Remove(PID);
		}

		public void dodajListeProcesow(Dictionary<int, Proces> lista)
		{
			listaProcesow = lista;
		}

		public void printProcessList()
		{
			foreach (var proces in listaProcesow)
			{
				Console.WriteLine("PID: " + proces.Key);
				Console.WriteLine("Nazwa: " + proces.Value.nazwa);
				Console.WriteLine("Pamięć: " + proces.Value.zuzyciePamieci);
			}
		}

		public void printProcessToFile(string filename = "procesy.txt")
		{
			if (System.IO.File.Exists(filename)) System.IO.File.Delete(filename);
			foreach (var proces in listaProcesow)
			{
				System.IO.File.AppendAllText(filename, String.Format("{0} | {1} | {2}\n", proces.Key, proces.Value.nazwa, proces.Value.zuzyciePamieci));
			}
		}
	}

	public class Proces
	{
		/// <summary>Nazwa procesu</summary>
		public string nazwa { get; private set; }

		/// <summary>Zużycie pamięci w [MB]</summary>
		public double zuzyciePamieci { get; private set; }

		public bool jestNaCzarnejLiscie { get; set; }

		/// <summary></summary>
		/// <param name="nazwa">Nazwa procesu</param>
		/// <param name="zuzyciePamieci">Zużycie pamięci w [MB]</param>
		public Proces(string nazwa, double zuzyciePamieci)
		{
			this.nazwa = nazwa;
			this.zuzyciePamieci = zuzyciePamieci;
			jestNaCzarnejLiscie = false;
		}
	}
}