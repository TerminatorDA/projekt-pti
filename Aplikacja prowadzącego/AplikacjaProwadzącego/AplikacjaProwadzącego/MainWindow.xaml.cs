﻿using PTI_TCP_Klient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using PTI_TCP;
using System.Net;
using System.Drawing;
using System.IO;

namespace AplikacjaProwadzącego
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		#region zmienneGlobalneKonstruktorMetodyPomocnicze

		private static int iloscKolumn = 0;
		public Klient klient;
		private string nazwaOknaPrefix = "SuperSpy200Pro";

		public List<String> p;

		public string aktywnaLista = "";
		
		[System.Runtime.InteropServices.DllImport("gdi32.dll")]
		public static extern bool DeleteObject(IntPtr hObject);

		public MainWindow()
		{
			InitializeComponent();

			Closing += closingEvent;
			
			DataContext = new Widok(new Dictionary<int, Proces> { });

			klient = Klient.startClient(12321, Klient.Protocol.UDP);
			klient.socket.EnableBroadcast = true;
			Task task = new Task(() => autoBroadcast(15000));
			task.Start();
		}

		private void closingEvent(object sender, System.ComponentModel.CancelEventArgs args)
		{
			MessageBoxResult result = MessageBox.Show("Czy na pewno chcesz zakończyć działanie aplikacji?", "Zamknięcie aplikacji", MessageBoxButton.YesNo, MessageBoxImage.Question);
			
			if (result == MessageBoxResult.No)
			{
				args.Cancel = true;
			}
			else
			{
				byte[] bytes = Protokol.newGetBytes(Protokol.Action.clientInactive, "Klient", "Admin");
				klient.sendBytes(bytes, IPAddress.Broadcast.ToString(), 15001);
				StreamWriter file = new System.IO.StreamWriter("black.txt");
				try
				{
					foreach (var f in ((App)Application.Current).blackList)
					{
						file.WriteLine(f.ToString());
					}
				}
				catch (Exception e)
				{
					Console.WriteLine("Nie udało się zapisać blackListy do pliku " + e.Message);
				}
				finally
				{
					file.Close();
				}
			}
		}
		
		/// <summary>Sprawia, że miniaturki na naszej liście zawsze pasują do niej jak ulał</summary>
		private void przeliczWielkoscOkna(object sender, RoutedEventArgs e)
		{
			double wysokosc = ((System.Windows.Controls.Panel)Application.Current.MainWindow.Content).ActualHeight / 4.25;
			double szerokosc = 1.5238095238 * wysokosc;

			ColumnDefinitionCollection kolumny = Pulpity.ColumnDefinitions;
			RowDefinitionCollection wiersze = Pulpity.RowDefinitions;

			foreach (ColumnDefinition kolumna in kolumny)
			{
				kolumna.Width = new GridLength(szerokosc);
			}

			wiersze[0].Height = new GridLength(wysokosc);
		}

		private void nazwijKolumny(object sender, RoutedEventArgs args)
		{
			((DataGrid)sender).Columns[0].Header = "Proces";
			((DataGrid)sender).Columns[1].Header = "Pamięć";
		}

		#endregion

		#region obslugaPrzyciskow

		/// <summary>Otwiera okno zarządzania Black Listą</summary>
		private void zarzadzajBlackLista(object sender, RoutedEventArgs e)
		{
			BlackList bl = new AplikacjaProwadzącego.BlackList();
			bl.Show();
		}

		/// <summary>Metoda do uruchamiania broadcastu rozkazującego połączyć się daemonom</summary>
		private void usunNieaktywneStanowiska(object sender, RoutedEventArgs e)
		{
			//byte[] bytes = Protokol.newGetBytes(Protokol.Action.clientActive, "Klient", "Admin");
			//klient.sendBytes(bytes, IPAddress.Broadcast.ToString(), 15001);
			usunNieaktywne();
		}

		/// <summary>Metoda do rozgłaszania zakazu dołączania daemonów</summary>
		private void wyslijDoWielu(object sender, RoutedEventArgs e)
		{
			//byte[] bytes = Protokol.newGetBytes(Protokol.Action.clientInactive, "Klient", "Admin");
			//klient.sendBytes(bytes, IPAddress.Broadcast.ToString(), 15001);

			Message msg = new Message("", true);
			msg.ShowDialog();
		}

		private void pokazStanowisko(object sender, RoutedEventArgs e)
		{
			RichButton button = (RichButton)sender;
			Stanowisko stanowisko = null;
			Application.Current.Dispatcher.Invoke(new Action(() =>
			{
				stanowisko = ((App)Application.Current).serwer.getStanowisko(button.Name);
			}));

			if(stanowisko == null)
			{
				usunStanowisko(button.Name);
				return;
			}

			using (Bitmap bmp = new System.Drawing.Bitmap(stanowisko.aktualnyZrzutEkranu))
			{
				IntPtr hBitmap = bmp.GetHbitmap();
				try
				{
					Podglad.Source = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(hBitmap, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
				}
				finally
				{
					DeleteObject(hBitmap);
				}
			}

			DataContext = new Widok(stanowisko.listaProcesow);

			this.Title = nazwaOknaPrefix + " - " + stanowisko.nazwaUzytkownika;

			aktywnaLista = button.Name;

			MenuProcesy.Visibility = Visibility.Visible;
		}

		#endregion

		#region obslugaStanowiskUI

		/// <summary>Tworzenie instancji nowego użytkownika w UC</summary>
		/// <param name="nazwaStanowiska">Nazwa stanowiska, którego instancja ma zostać utworzona</param>
		public void noweStanowisko(string nazwaStanowiska)
		{
			Stanowisko stanowisko = null;

			Application.Current.Dispatcher.Invoke(new Action(() =>
			{
				stanowisko = ((App)Application.Current).serwer.getStanowisko(nazwaStanowiska);
			}));

			if (stanowisko == null) return;

			RichButton Obraz = new RichButton();
			
			using(Bitmap bmp = new System.Drawing.Bitmap(stanowisko.aktualnyZrzutEkranu))
			{
				IntPtr hBitmap = bmp.GetHbitmap();
				try
				{
					Obraz.SSpulpitu.Source = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(hBitmap, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
				}
				finally
				{
					DeleteObject(hBitmap);
				}
			}
			
			Obraz.NazwaStanowiska.Content = stanowisko.nazwaUzytkownika;

			Obraz.Name = stanowisko.nazwaKomputera;

			Obraz.Margin = new Thickness(5, 5, 5, 5);

			ustawAlert(Obraz, Alert.None);

			if (Pulpity.FindName(Obraz.Name) == null)
			{
				Obraz.MouseLeftButtonDown += pokazStanowisko;
				Pulpity.RegisterName(Obraz.Name, Obraz);
				Grid.SetRow(Obraz, 0);
				Grid.SetColumn(Obraz, iloscKolumn);
				Pulpity.Children.Add(Obraz);
				Pulpity.ColumnDefinitions.Add(new ColumnDefinition());
				iloscKolumn++;
				przeliczWielkoscOkna(this, new RoutedEventArgs());
			}
			else
			{
				RichButton button = (RichButton)Pulpity.FindName(Obraz.Name);
				button = Obraz;
			}

			

			//DataContext = new Widok(stanowisko.listaProcesow);
		}

		public enum Alert { None, Warning, Disconnect, Stop }

		public void ustawAlert(string nazwaStanowiska, Alert alert)
		{
			RichButton button = (RichButton)Pulpity.FindName(nazwaStanowiska);
			ustawAlert(button, alert);
		}

		public void ustawAlert(RichButton button, Alert alert)
		{
			switch (alert)
			{
				case Alert.Warning:
					button.Alert.Source = new BitmapImage(new Uri(@"Images\alert_red.png", UriKind.Relative));
					button.AlertTip.Content = "Proces z czarnej listy jest aktywny.";
					break;
				case Alert.Disconnect:
					button.Alert.Source = new BitmapImage(new Uri(@"Images\alert_yellow.png", UriKind.Relative));
					button.AlertTip.Content = "Brak połączenia ze stanowiskiem.";
					break;
				case Alert.Stop:
					button.Alert.Source = new BitmapImage(new Uri(@"Images\alert_blue.png", UriKind.Relative));
					button.AlertTip.Content = "Stanowisko nie wysyła pakietów. Wznów wysyłanie, aby zaktualizować dane.";
					break;
				default:
					button.Alert.Source = null;
					button.AlertTip.Content = "";
					break;
			}
		}

		public Alert getAlert(string nazwaStanowiska)
		{
			RichButton button = (RichButton)Pulpity.FindName(nazwaStanowiska);
			if((string)button.AlertTip.Content == "Proces z czarnej listy jest aktywny.")
			{
				return Alert.Warning;
			}
			if ((string)button.AlertTip.Content == "Brak połączenia ze stanowiskiem.")
			{
				return Alert.Disconnect;
			}
			if ((string)button.AlertTip.Content == "Stanowisko nie wysyła pakietów. Wznów wysyłanie, aby zaktualizować dane.")
			{
				return Alert.Stop;
			}
			else
			{
				return Alert.None;
			}
		}

		private void usunNieaktywne()
		{
			List<IPEndPoint> listaDoUsuniecia = new List<IPEndPoint>();
			Application.Current.Dispatcher.Invoke(new Action(() =>
			{
				foreach(var stanowisko in ((App)Application.Current).serwer.listaStanowisk)
				{
					if(getAlert(stanowisko.Value.nazwaKomputera) == Alert.Disconnect)
					{
						listaDoUsuniecia.Add(stanowisko.Key);
					}
				}
			}));
			foreach(var ip in listaDoUsuniecia)
			{
				((App)Application.Current).serwer.usunStanowisko(ip,
					((App)Application.Current).serwer.listaSocketow[ip]);
			}
			listaDoUsuniecia.Clear();
			listaDoUsuniecia = null;
		}

		/// <summary>Odświeza widok stanowiska</summary>
		/// <param name="nazwaStanowiska">Nazwa stanowiska, którego ekran ma zostać odświezony</param>
		public void odswiezStanowisko(string nazwaStanowiska)
		{
			Application.Current.Dispatcher.Invoke(new Action(() =>
			{
				Stanowisko stanowisko = null;
				stanowisko = ((App)Application.Current).serwer.getStanowisko(nazwaStanowiska);

				if (stanowisko == null) return;
				using (Bitmap bmp = new System.Drawing.Bitmap(stanowisko.aktualnyZrzutEkranu))
				{
					IntPtr hBitmap = bmp.GetHbitmap();
					try
					{
						((RichButton)Pulpity.FindName(stanowisko.nazwaKomputera)).SSpulpitu.Source = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(hBitmap, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
						if (nazwaStanowiska == aktywnaLista)
						{
							DataContext = new Widok(stanowisko.listaProcesow);

							Podglad.Source = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(hBitmap, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
						}
					}
					//catch (NullReferenceException e)
					//{
					//	Console.WriteLine("Dziwny null reference przy odswiezaniu: " + e.Message);
					//}
					finally
					{
						DeleteObject(hBitmap);
					}
				}
			}));
		}

		/// <summary>Usuwa stanowisko z listy</summary>
		/// <param name="computerName">Nazwa stanowiska, które ma zostać usunięte z listy</param>
		public void usunStanowisko(string computerName)
		{
			RichButton stanowisko = (RichButton)Pulpity.FindName(computerName);
			if (stanowisko == null) return;
			Pulpity.ColumnDefinitions.RemoveAt(System.Windows.Controls.Grid.GetColumn(stanowisko));
			Pulpity.Children.Remove(stanowisko);
			Pulpity.UnregisterName(computerName);
			sortujStanowiska(Grid.GetColumn(stanowisko));
			iloscKolumn--;
			if (aktywnaLista == computerName)
			{
				wylaczAktywneStanowisko();
			}
		}
		
		/// <summary>Sortuje wszystkie elementy "Pulpitów" po usunięciu jednego elementu</summary>
		/// <param name="kolumna">Kolumna, w której znajdował się usunięty element</param>
		private void sortujStanowiska(int kolumna)
		{
			kolumna++;

			// Iteracja do wyczerpania elementów
			for (; kolumna <= iloscKolumn; kolumna++)
			{
				try
				{
					String nazwa = ((RichButton)Pulpity.Children.Cast<UIElement>().First(e => Grid.GetColumn(e) == kolumna)).Name;

					Grid.SetColumn((RichButton)Pulpity.FindName(nazwa), kolumna - 1);
				}
				catch (System.InvalidOperationException e)
				{
					continue;
				}
			}
		}

		#endregion

		#region broadcast

		public void wyslijWiadomosc(string nazwaKomputera, string wiadomosc, string tytul, string autor)
		{
			Application.Current.Dispatcher.Invoke(new Action(() =>
			{
				IPEndPoint ip = ((App)Application.Current).serwer.listaStanowisk.FirstOrDefault(x => x.Value.nazwaKomputera == nazwaKomputera).Key;
				if (ip == null) return;
				wiadomosc = wiadomosc.Replace("|", "");
				tytul = tytul.Replace("|", "");
				autor = autor.Replace("|", "");
				string pelnaWiadomosc = autor + "|" + tytul + "|" + wiadomosc;
				string dataString = Protokol.getDataString(Protokol.DataType.message, pelnaWiadomosc);
				byte[] bytes = Protokol.newGetBytes(Protokol.Action.sendMessage, "Klient", "Admin", dataString);
				klient.sendBytes(bytes, ip.Address.ToString(), 15001);
			}));
		}

		public void wylaczWysylanie(string nazwaKomputera)
		{
			Application.Current.Dispatcher.Invoke(new Action(() =>
			{
				IPEndPoint ip = ((App)Application.Current).serwer.listaStanowisk.FirstOrDefault(x => x.Value.nazwaKomputera == nazwaKomputera).Key;
				if (ip == null) return;
				byte[] bytes = Protokol.newGetBytes(Protokol.Action.stopSending, "Klient", "Admin", null);
				klient.sendBytes(bytes, ip.Address.ToString(), 15001);
			}));
		}

		public void wlaczWysylanie(string nazwaKomputera)
		{
			Application.Current.Dispatcher.Invoke(new Action(() =>
			{
				IPEndPoint ip = ((App)Application.Current).serwer.listaStanowisk.FirstOrDefault(x => x.Value.nazwaKomputera == nazwaKomputera).Key;
				if (ip == null) return;
				byte[] bytes = Protokol.newGetBytes(Protokol.Action.startSending, "Klient", "Admin", null);
				klient.sendBytes(bytes, ip.Address.ToString(), 15001);
			}));
		}

		private async void autoBroadcast(int delay)
		{
			while (true)
			{
				byte[] bytes = Protokol.newGetBytes(Protokol.Action.clientActive, "Klient", "Admin");
				klient.sendBytes(bytes, IPAddress.Broadcast.ToString(), 15001);
				await Task.Delay(delay);
			}
		}
		#endregion

		private void EkranOff_Click(object sender, RoutedEventArgs e)
		{
			wylaczAktywneStanowisko();
		}

		private void wylaczAktywneStanowisko()
		{
			Podglad.Source = null;
			DataContext = new Widok(new Dictionary<int, Proces>());
			this.Title = nazwaOknaPrefix;
			aktywnaLista = "";
			MenuProcesy.Visibility = Visibility.Collapsed;
		}

		private void DodajDoBL_Click(object sender, RoutedEventArgs e)
		{
			if (aktywnaLista == "") return;
			try
			{
				string nazwaProcesu = ((Proces)Procesy.CurrentItem).nazwa;
				if ((string)DodajDoBL.Header == "Dodaj proces do czarnej listy" && 
					!((App)(Application.Current)).blackList.Contains(nazwaProcesu))
				{
					((App)(Application.Current)).blackList.Add(nazwaProcesu);
					((Proces)Procesy.CurrentItem).jestNaCzarnejLiscie = true;
					Procesy.Items.Refresh();
				}
				else if ((string)DodajDoBL.Header == "Usuń proces z czarnej listy" &&
					((App)(Application.Current)).blackList.Contains(nazwaProcesu))
				{
					((App)(Application.Current)).blackList.Remove(nazwaProcesu);
					((Proces)Procesy.CurrentItem).jestNaCzarnejLiscie = false;
					Procesy.Items.Refresh();
				}
			}
			catch (NullReferenceException) { }
		}

		private void Procesy_ContextMenuOpening(object sender, ContextMenuEventArgs e)
		{
			try
			{
				string nazwaProcesu = ((Proces)Procesy.CurrentItem).nazwa;
				if (((App)(Application.Current)).blackList.Contains(nazwaProcesu))
					DodajDoBL.Header = "Usuń proces z czarnej listy";
				else
					DodajDoBL.Header = "Dodaj proces do czarnej listy";
			}
			catch (NullReferenceException) { }
		}
	}
}