﻿using PTI_TCP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AplikacjaProwadzącego
{
	/// <summary>
	/// Interaction logic for RichButton.xaml
	/// </summary>
	public partial class RichButton : UserControl
	{
		public RichButton()
		{
			InitializeComponent();
		}

		public System.Drawing.Image screenSchot
		{
			get { return base.GetValue(SourceProperty) as System.Drawing.Image; }
			set { base.SetValue(SourceProperty, value); }
		}

		public static readonly RoutedEvent ClickEvent =
			EventManager.RegisterRoutedEvent("Click", RoutingStrategy.Bubble,
			typeof(RoutedEventHandler), typeof(RichButton));

		public event RoutedEventHandler Click
		{
			add { AddHandler(ClickEvent, value); }
			remove { RemoveHandler(ClickEvent, value); }
		}

		public Label nazwaStanowiska;

		public static readonly DependencyProperty SourceProperty = DependencyProperty.Register("Source", typeof(System.Drawing.Image), typeof(Widok));
		
		private void MenuWysylanie_Click(object sender, RoutedEventArgs e)
		{
			if ((string)MenuWysylanie.Header == "Przestań odbierać dane")
			{
				MenuWysylanie.Header = "Zacznij odbierać dane";
				Application.Current.Dispatcher.Invoke(new Action(() =>
				{
					((MainWindow)(Application.Current.MainWindow)).wylaczWysylanie(this.Name);
					((MainWindow)(Application.Current.MainWindow)).ustawAlert(this, MainWindow.Alert.Stop);
				}));
			}
			else
			{
				MenuWysylanie.Header = "Przestań odbierać dane";
				Application.Current.Dispatcher.Invoke(new Action(() =>
				{
					((MainWindow)(Application.Current.MainWindow)).wlaczWysylanie(this.Name);
					((MainWindow)(Application.Current.MainWindow)).ustawAlert(this, MainWindow.Alert.None);
				}));
			}
		}
		
		private void MenuWiadomosc_Click(object sender, RoutedEventArgs e)
		{
			Message msg = new Message(this.Name, false);
			msg.ShowDialog();
		}

		private void MenuUsun_Click(object sender, RoutedEventArgs e)
		{
			Application.Current.Dispatcher.Invoke(new Action(() =>
			{
				IPEndPoint ip = ((App)(Application.Current)).serwer.getKey(this.Name);
				((App)(Application.Current)).serwer.usunStanowisko(ip, ((App)(Application.Current)).serwer.listaSocketow[ip]);
			}));
		}

		private void MainRichButtonGrid_ContextMenuOpening(object sender, ContextMenuEventArgs e)
		{
			Application.Current.Dispatcher.Invoke(new Action(() =>
			{
				if(((MainWindow)(Application.Current.MainWindow)).getAlert(this.Name) == MainWindow.Alert.Disconnect)
				{
					MenuWiadomosc.IsEnabled = false;
					MenuWysylanie.IsEnabled = false;
					MenuUsun.IsEnabled = true;
				}
				else
				{
					MenuWiadomosc.IsEnabled = true;
					MenuWysylanie.IsEnabled = true;
					MenuUsun.IsEnabled = false;
				}
			}));
		}
	}
}