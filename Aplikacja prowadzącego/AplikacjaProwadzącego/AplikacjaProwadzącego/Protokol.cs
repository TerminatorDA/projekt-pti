﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace PTI_TCP
{
	static class Protokol
	{
		public enum Action
		{
			/// <summary>Nieznana akcja</summary>
			none,
			/// <summary>Akcja wysyłania danych</summary>
			sendingData,
			/// <summary>Poinformowanie o aktywności klienta</summary>
			clientActive,
			/// <summary>Poinformowanie o braku aktywności klienta</summary>
			clientInactive,
			/// <summary>Poinformowanie o wysyłaniu danych do klienta</summary>
			startSending,
			/// <summary>Poinformowanie o niewysyłaniu danych do klienta</summary>
			stopSending,
			/// <summary>Wysłanie danych o zabiciu procesów</summary>
			killProcesses,
			/// <summary>Wysłanie wiadomości</summary>
			sendMessage,
			/// <summary>Wysłanie informacji o wyłączeniu klienta</summary>
			byeServer,
			/// <summary>Wysłanie informacji o wyłączeniu serwera</summary>
			byeClient,
			/// <summary>Włączenie trybu szybkiego wysyłania</summary>
			fastMode,
			/// <summary>Wyłączenie trybu szybkiego wysyłania</summary>
			slowMode
		}

		public enum DataType
		{
			/// <summary>Nieznany typ</summary>
			none,
			/// <summary>Lista procesów</summary>
			processList,
			/// <summary>Zrzut ekranu</summary>
			screenshot,
			/// <summary>Wiadomość</summary>
			message
		}
		
		/// <summary>Tworzy tablicę bajtów, gotową do wysłania</summary>
		/// <param name="action">Akcja, jaką dane żądanie ma wykonać</param>
		/// <param name="computerID">Nazwa stanowiska</param>
		/// <param name="userID">Nazwa użytkownika</param>
		/// <param name="data">Ciąg danych wysyłanych przy akcji sendingData</param>
		/// <returns></returns>
		public static byte[] newGetBytes(Action action, string computerID, string userID, string data = null)
		{
			Random rnd = new Random();
			int seq = rnd.Next();
			StringBuilder builder = new StringBuilder();
			builder.Append(((int)action).ToString() + "|" + computerID + "|" + userID);
			if (action == Action.sendingData && data != null)
			{
				builder.Append(data);
			}
			else if (action == Action.sendMessage && data != null)
			{
				builder.Append(data);
			}
			builder.Append("|###");

			byte[] bytes;
			bytes = Encoding.UTF8.GetBytes(builder.ToString());
			return bytes;
		}

		/// <summary>Zwraca dane w postaci stringa.</summary>
		/// <param name="dataType">Typ danych do zakodowania</param>
		/// <param name="data">Dane do zakodowania</param>
		/// <returns></returns>
		public static string getDataString(DataType dataType, Object data)
		{
			StringBuilder builder = new StringBuilder();
			if (dataType == DataType.processList && data.GetType() == typeof(Dictionary<int, Proces>))
			{
				Dictionary<int, Proces> newData = data as Dictionary<int, Proces>;
				int length = newData.Count;
				builder.Append("|" + ((int)dataType).ToString() + "|" + length.ToString());
				foreach (var process in newData)
				{
					builder.Append("|" + process.Key + "|" +
						process.Value.nazwa + "|" +
						process.Value.zuzyciePamieci.ToString());
				}
			}
			else if (dataType == DataType.screenshot && data.GetType() == typeof(System.Drawing.Bitmap))
			{
				Image newData = data as Image;
				builder.Append("|" + ((int)dataType).ToString() + "|");
				string image = imageToString(newData);
				builder.Append(image.Length.ToString() + "|" + image);
			}
			else if (dataType == DataType.message && data.GetType() == typeof(string))
			{
				string newData = data as string;
				builder.Append("|" + ((int)dataType).ToString() + "|");
				builder.Append(newData.Length.ToString() + "|" + newData);
			}
			return builder.ToString();
		}
		
		/// <summary>Zwraca nazwę akcji z otrzymanego pakietu</summary>
		/// <param name="data">Łańcuch znaków bajtów otrzymanych podczas komunikacji</param>
		/// <returns></returns>
		public static Action getAction(string data)
		{
			data = data.Substring(0, data.IndexOf('|'));
			try
			{
				return (Action)Convert.ToInt32(data);
			}
			catch (Exception)
			{
				return Action.none;
			}
		}
		
		/// <summary>Zwraca nazwę stanowiska z otrzymanego pakietu</summary>
		/// <param name="data">Łańcuch znaków bajtów, otrzymanych podczas komunikacji</param>
		/// <returns></returns>
		public static string getComputerName(string data)
		{
			int _od = data.IndexOf('|') + 1;
			int _do = data.IndexOf('|', _od);
			data = data.Substring(_od, (_do - _od));
			return data;
		}

		/// <summary>Zwraca nazwę użytkownika z otrzymanego pakietu</summary>
		/// <param name="data">Łańcuch znaków bajtów, otrzymanych podczas komunikacji</param>
		/// <returns></returns>
		public static string getUserName(string data)
		{
			int _od = data.IndexOf('|', data.IndexOf('|') + 1) + 1;
			int _do = data.IndexOf('|', _od);
			data = data.Substring(_od, (_do - _od));
			return data;
		}
		
		/// <summary>Zwraca listę danych z otrzymanego pakietu</summary>
		/// <param name="data">Łańcuch znaków bajtów, otrzymanych podczas komunikacji</param>
		/// <returns></returns>
		public static List<string> getData(string data)
		{
			string[] dataArray = data.Split("|".ToCharArray());
			List<string> lista = new List<string>();
			if (dataArray[0] == "1")
			{
				//string computerID = dataArray[1];
				int i = 3;
				while (dataArray[i] != "###")
				{
					StringBuilder builder = new StringBuilder();
					if (dataArray[i] == "1") // lista procesow
					{
						builder.Append(dataArray[i] + "|");
						int length = Convert.ToInt32(dataArray[++i]);
						builder.Append(dataArray[i]);
						for (int j = 0; j < length; j++)
						{
							builder.Append("|" + dataArray[i + 1] +
								"|" + dataArray[i + 2] +
								"|" + dataArray[i + 3]);
							i += 3;
						}
						i++;
					}
					else if (dataArray[i] == "2") // zrzut ekranu
					{
						builder.Append(dataArray[i] + "|");
						int length = Convert.ToInt32(dataArray[++i]);
						builder.Append(dataArray[i] + "|");
						builder.Append(dataArray[++i]);
						i++;
					}
					else if (dataArray[i] == "3") // wiadomosc
					{
						builder.Append(dataArray[i] + "|");
						builder.Append(dataArray[++i] + "|");
						builder.Append(dataArray[++i]);
						i++;
					}
					else i++;
					lista.Add(builder.ToString());
				}
			}
			return lista;
		}

		public static string getFullPacket(byte[][] bytes)
		{
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.Length; i++)
			{
				string temp = Encoding.UTF8.GetString(bytes[i]);
				temp = temp.Remove(0, temp.IndexOf((char)3) + 1);
				sb.Append(temp);
			}
			return sb.ToString();
		}

		public static string getFullPacket(string[] data)
		{
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < data.Length; i++)
			{
				string temp = data[i];
				temp = temp.Remove(0, temp.IndexOf((char)3) + 1);
				sb.Append(temp);
			}
			return sb.ToString();
		}

		public static int[] getPacketInfo(byte[] bytes)
		{
			string temp = Encoding.UTF8.GetString(bytes);
			return getPacketInfo(temp);
		}

		public static int[] getPacketInfo(string data)
		{
			int[] info = new int[3];
			data = data.Remove(data.IndexOf((char)3));
			string[] splittedInfo = data.Split("|".ToCharArray());
			for (int i = 0; i < 3; i++)
			{
				try
				{
					info[i] = Convert.ToInt32(splittedInfo[i]);
				}
				catch (FormatException ex)
				{
					info[i] = -1;
				}
			}
			return info;
		}

		/// <summary>Zwraca słownik procesów z otrzymanego łańcucha danych processList</summary>
		/// <param name="data">Łańcuch danych otrzymany podczas metody getData</param>
		/// <returns></returns>
		public static Dictionary<int, Proces> getProcessList(string data)
		{
			string[] dataArray = data.Split("|".ToCharArray());
			Dictionary<int, Proces> listaProcesow = new Dictionary<int, Proces>();
			if (dataArray[0] == "1")
			{
				int length = Convert.ToInt32(dataArray[1]);
				int k = 2;
				
				for (int i = 0; i < length; i++)
				{
					int id = Convert.ToInt32(dataArray[k++]);
					string nazwa = dataArray[k++];
					double pamiec = Convert.ToDouble(dataArray[k++].Replace(".", ","));
					Proces newProces = new Proces(nazwa, pamiec);
					listaProcesow[id] = newProces;
				}
			}
			return listaProcesow;
		}

		/// <summary>Zwraca obrazek z danych typu screenshot</summary>
		/// <param name="data">Łańcuch danych otrzymany podczas metody getData</param>
		/// <returns></returns>
		public static Image getImage(string data)
		{
			string[] dataArray = data.Split("|".ToCharArray());
			Image image = null;
			if (dataArray[0] == "2" && dataArray.Length >= 3)
			{
				int length = Convert.ToInt32(dataArray[1]);
				if (length != dataArray[2].Length) // prawdopodobnie obrazek jest niepelny
					image = Image.FromFile("Images/error-image.jpg");
				image = stringToImage(dataArray[2]);
			}
			else
				image = Image.FromFile("Images/error-image.jpg");
			return image;
		}

		public static string getMessage(string data)
		{
			string[] dataArray = data.Split("|".ToCharArray());
			if (dataArray.Length >= 3)
				return dataArray[2];
			else return null;
		}

		/// <summary>Zwraca typ danych podanego łańcucha danych</summary>
		/// <param name="data">Łańcuch danych otrzymany podczas metody getData</param>
		/// <returns></returns>
		public static DataType getDataType(string data)
		{
			int index = data.IndexOf('|');
			if(index < 0) return DataType.none;
			data = data.Substring(0, index);
			try
			{
				return (DataType)Convert.ToInt32(data);
			}
			catch (Exception)
			{
				return DataType.none;
			}
		}

		private static string imageToString(Image image)
		{
			string str = null;
			using (MemoryStream ms = new MemoryStream())
			{
				image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg); // image.RawFormat
				byte[] bytes = ms.ToArray();
				str = Convert.ToBase64String(bytes);
			}
			return str;
		}

		private static Image stringToImage(string str)
		{
			Image image = null;
			try
			{
				byte[] bytes = Convert.FromBase64String(str);
				MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length);
				ms.Write(bytes, 0, bytes.Length);
				image = Image.FromStream(ms, true);
			}
			catch (FormatException)
			{
				image = Image.FromFile("Images/error-image.jpg");
			}
			return image;
		}

		public static List<string> getListOfPackets(byte[] data)
		{
			List<string> list = new List<string>();
			string dataString = Encoding.UTF8.GetString(data);
			int lastIndex = 0;
			int indexOfEof;// = dataString.IndexOf("|###", lastIndex);
			while (lastIndex < dataString.Length)
			{
				indexOfEof = dataString.IndexOf("|###", lastIndex);
				if (indexOfEof == -1) return list;
				indexOfEof += 4;
				string temp = dataString.Substring(lastIndex, (indexOfEof - lastIndex));
				lastIndex = indexOfEof;
				list.Add(temp);
			}
			return list;
		}
	}
}
