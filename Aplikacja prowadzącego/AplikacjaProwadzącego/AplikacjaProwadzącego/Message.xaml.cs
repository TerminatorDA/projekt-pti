﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AplikacjaProwadzącego
{
	/// <summary>
	/// Interaction logic for Message.xaml
	/// </summary>
	public partial class Message : Window
	{
		public Message(string nazwaStanowiska, bool trybMulti)
		{
			InitializeComponent();
			nazwa = nazwaStanowiska;
			this.trybMulti = trybMulti;
			if(trybMulti)
				this.Title = "Wiadomość do wielu";
			else
				this.Title = "Wiadomość do " + nazwa;
		}

		private string nazwa;
		private bool trybMulti;

		private void Wyslij_Click(object sender, RoutedEventArgs e)
		{
			//string autor = AuthorBox.Text;
			string autor = "Admin";
			//if(autor.Length > 32)
			//{
			//	MessageBox.Show("Nazwa autora jest zbyt długa", "Błąd", MessageBoxButton.OK, MessageBoxImage.Stop);
			//}
			string tytul = TitleBox.Text;
			if (tytul.Length > 32)
			{
				MessageBox.Show("Tytuł jest zbyt długi", "Błąd", MessageBoxButton.OK, MessageBoxImage.Stop);
				return;
			}
			string wiadomosc = MessageTBox.Text;
			if (wiadomosc.Length > 255)
			{
				MessageBox.Show("Wiadomość jest zbyt długa", "Błąd", MessageBoxButton.OK, MessageBoxImage.Stop);
				return;
			}
			Application.Current.Dispatcher.Invoke(new Action(() =>
			{
				if (trybMulti)
				{
					foreach(var stanowisko in ((App)Application.Current).serwer.listaStanowisk)
					{
						((MainWindow)(Application.Current.MainWindow)).wyslijWiadomosc(stanowisko.Value.nazwaKomputera, wiadomosc, tytul, autor);
					}
				}
				else
					((MainWindow)(Application.Current.MainWindow)).wyslijWiadomosc(nazwa, wiadomosc, tytul, autor);
			}));
			this.Close();
		}
	}
}
