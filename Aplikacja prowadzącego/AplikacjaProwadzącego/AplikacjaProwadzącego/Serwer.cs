﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Windows;
using AplikacjaProwadzącego;
using System.Windows.Threading;
using System.Diagnostics;

namespace PTI_TCP
{
	public class SerwerTCP : Serwer
	{
		/// <summary>Lista stanowisk w laboratorium</summary>
		public Dictionary<IPEndPoint, Stanowisko> listaStanowisk { get; private set; }
		private Dictionary<IPEndPoint, string> lostData;
		public Dictionary<IPEndPoint, Socket> listaSocketow { get; private set; }

		public SerwerTCP() : base()
		{
			listaStanowisk = new Dictionary<IPEndPoint, Stanowisko>();
			lostData = new Dictionary<IPEndPoint, string>();
			listaSocketow = new Dictionary<IPEndPoint, Socket>();
		}

		public List<string> getListOfPackets(IPEndPoint ip, byte[] data)
		{
			List<string> list = new List<string>();

			string dataString = Encoding.UTF8.GetString(data);
			if (lostData.ContainsKey(ip))
				dataString = lostData[ip] + dataString;
			lostData[ip] = "";
			int lastIndex = 0;
			int indexOfEof;
			while (lastIndex < dataString.Length)
			{
				indexOfEof = dataString.IndexOf("|###", lastIndex);

				if (indexOfEof == -1)
				{
					lostData[ip] = dataString.Substring(lastIndex);
					return list;
				}

				indexOfEof += 4;
				string temp = dataString.Substring(lastIndex, (indexOfEof - lastIndex));
				lastIndex = indexOfEof;
				list.Add(temp);
			}
			return list;
		}

		override public void doAfterReceive(IPEndPoint ip, byte[] data, Socket tcpSocket)
		{
			Console.WriteLine("Dostałem pakiet o wielkości: " + data.Length + " bajtów");
			List<string> listaPakietow = getListOfPackets(ip, data);
			Console.WriteLine("Podzieliłem go na: " + listaPakietow.Count);
			foreach (var pakiet in listaPakietow)
			{
				Protokol.Action akcja = Protokol.getAction(pakiet);
				string computerName = Protokol.getComputerName(pakiet);
				computerName = adjustName(computerName);
				string userName = Protokol.getUserName(pakiet);


				if (akcja == Protokol.Action.sendingData)
				{
					bool noweStanowisko = false;
					if (!listaStanowisk.ContainsKey(ip))
					{
						// DODAWANIE NOWEGO STANOWISKA DO LISTY STANOWISK
						Stanowisko nowe = new Stanowisko(computerName, userName);
						listaStanowisk[ip] = nowe;
						noweStanowisko = true;
					}

					List<string> listaDanych = Protokol.getData(pakiet);
					foreach (var dana in listaDanych)
					{
						if (Protokol.getDataType(dana) == Protokol.DataType.screenshot)
						{
							listaStanowisk[ip].aktualnyZrzutEkranu = Protokol.getImage(dana);
						}
						else if (Protokol.getDataType(dana) == Protokol.DataType.processList)
						{
							listaStanowisk[ip].dodajListeProcesow(Protokol.getProcessList(dana));
						}
					}

					if (noweStanowisko)
					{
						// WYSWIETLANIE NOWEGO STANOWISKA W PANELU
						Application.Current.Dispatcher.Invoke(new Action(() =>
						{
							try
							{
								((MainWindow)System.Windows.Application.Current.MainWindow).noweStanowisko(computerName);
							}
							catch (NullReferenceException) { } // czasami gdy zamyka się program Application.Current == null
						}));
					}

					bool warning = false;
					Application.Current.Dispatcher.Invoke(new Action(() =>
					{
						foreach (var proces in listaStanowisk[ip].listaProcesow)
						{
							if (((App)Application.Current).blackList.Contains(proces.Value.nazwa))
							{
								listaStanowisk[ip].listaProcesow[proces.Key].jestNaCzarnejLiscie = true;
								warning = true;
							}
						}
						if (warning)
						{
							((MainWindow)Application.Current.MainWindow).ustawAlert(computerName, MainWindow.Alert.Warning);
						}
						else
						{
							((MainWindow)Application.Current.MainWindow).ustawAlert(computerName, MainWindow.Alert.None);
						}
					}));

					if(!noweStanowisko)
					{
						// ODŚWIEŻENIE STANOWISKA W PANELU
						Application.Current.Dispatcher.Invoke(new Action(() =>
						{
							if(((MainWindow)Application.Current.MainWindow).getAlert(computerName) == MainWindow.Alert.Disconnect)
							{
								((MainWindow)Application.Current.MainWindow).ustawAlert(computerName, MainWindow.Alert.None);
							}
							try
							{
								((MainWindow)System.Windows.Application.Current.MainWindow).odswiezStanowisko(computerName);
							}
							catch (NullReferenceException) { } // czasami gdy zamyka się program Application.Current == null
						}));
					}

				}
				else if (akcja == Protokol.Action.byeServer)
				{
					Console.WriteLine("Dostałem BYE");
					doAfterDisconnect(ip, tcpSocket);
				}
				else
				{
					Console.WriteLine("Dostałem jakiś zbłąkakny pakiet");
				}
			}
		}

		public override void doAfterConnect(IPEndPoint ip, Socket tcpSocket)
		{
			// co się dzieje jak ktoś się połączy?
			Console.WriteLine("Użytkownik: " + ip + "połączył się.");
			listaSocketow[ip] = tcpSocket;
		}

		public override void doAfterDisconnect(IPEndPoint ip, Socket tcpSocket)
		{
			if (listaStanowisk.ContainsKey(ip))
			{
				string computerName = listaStanowisk[ip].nazwaKomputera;


				Application.Current.Dispatcher.Invoke(new Action(() =>
				{
					((MainWindow)System.Windows.Application.Current.MainWindow).ustawAlert(computerName, MainWindow.Alert.Disconnect);
				}));

				//Application.Current.Dispatcher.Invoke(new Action(() =>
				//{
				//	byte[] bytes = Protokol.newGetBytes(Protokol.Action.clientInactive, "Klient", "Admin");
				//	((MainWindow)Application.Current.MainWindow).klient.sendBytes(bytes, ip.Address.ToString(), 15001);
				//	((MainWindow)Application.Current.MainWindow).usunStanowisko(computerName);
				//}));

				//listaStanowisk.Remove(ip);
			}
			//try
			//{
			//	tcpSocket.Shutdown(SocketShutdown.Both);
			//	tcpSocket.Close();
			//}
			//catch (ObjectDisposedException) { }

		}

		public void usunStanowisko(IPEndPoint ip, Socket tcpSocket)
		{
			if (listaStanowisk.ContainsKey(ip))
			{
				string computerName = listaStanowisk[ip].nazwaKomputera;

				Application.Current.Dispatcher.Invoke(new Action(() =>
				{
					byte[] bytes = Protokol.newGetBytes(Protokol.Action.clientInactive, "Klient", "Admin");
					((MainWindow)Application.Current.MainWindow).klient.sendBytes(bytes, ip.Address.ToString(), 15001);
					((MainWindow)Application.Current.MainWindow).usunStanowisko(computerName);
				}));

				listaStanowisk.Remove(ip);
				if(listaSocketow.ContainsKey(ip))
					listaSocketow.Remove(ip);
			}
			try
			{
				tcpSocket.Shutdown(SocketShutdown.Both);
				tcpSocket.Close();
			}
			catch (ObjectDisposedException) { }
		}

		public Stanowisko getStanowisko(string nazwaStanowiska)
		{
			Stanowisko ostatnieStanowisko = null;
			List<IPEndPoint> listaKluczy = new List<IPEndPoint>();
			int counter = 0;
			foreach (var stanowisko in listaStanowisk)
			{
				if (stanowisko.Value.nazwaKomputera == nazwaStanowiska)
				{
					ostatnieStanowisko = stanowisko.Value;
					listaKluczy.Add(stanowisko.Key);
					counter++;
				}
			}
			if (counter > 1)
			{
				int i = 0;
				while (counter <= 1)
				{
					listaStanowisk.Remove(listaKluczy[i]);
					counter--;
					i++;
				}
			}
			return ostatnieStanowisko;
		}

		public IPEndPoint getKey(string nazwaStanowiska)
		{
			foreach (var stanowisko in listaStanowisk)
			{
				if (stanowisko.Value.nazwaKomputera == nazwaStanowiska)
				{
					return stanowisko.Key;
				}
			}
			return null;
		}

		public string adjustName(string oldName)
		{
			string newName = oldName.Replace((char)45, '_');

			return newName;
		}
	}

	public abstract class Serwer
	{
		private static readonly object _lock = new object();
		public enum Protocol
		{
			/// <summary>Protokół UDP</summary>
			UDP,
			/// <summary>Protokół TCP</summary>
			TCP
		}

		//EVENTY
		#region
		public delegate void ServerDelegate(object sender, EventArgs e);
		public event ServerDelegate receiving;
		public event ServerDelegate connecting;
		public event ServerDelegate disconnecting;

		public class ServerEvents : EventArgs
		{
			public byte[] data { get; private set; }
			public IPEndPoint ip { get; private set; }
			public Socket tcpSocket { get; private set; }

			public ServerEvents(IPEndPoint ip, byte[] data, Socket socket)
			{
				this.ip = ip;
				this.data = data;
				this.tcpSocket = socket;
			}
		}

		abstract public void doAfterReceive(IPEndPoint ip, byte[] data, Socket tcpSocket);
		abstract public void doAfterConnect(IPEndPoint ip, Socket tcpSocket);
		abstract public void doAfterDisconnect(IPEndPoint ip, Socket tcpSocket);
		private void closeSocket(Socket tcpSocket)
		{
			if (tcpSocket.Connected)
			{
				tcpSocket.Shutdown(SocketShutdown.Both);
				tcpSocket.Close();
			}
		}
		private void dataReceived(object sender, EventArgs e)
		{
			ServerEvents serverArgs = e as ServerEvents;
			doAfterReceive(serverArgs.ip, serverArgs.data, serverArgs.tcpSocket);
		}

		private void newConnection(object sender, EventArgs e)
		{
			ServerEvents serverArgs = e as ServerEvents;
			doAfterConnect(serverArgs.ip, serverArgs.tcpSocket);
		}

		private void connectionClosed(object sender, EventArgs e)
		{
			ServerEvents serverArgs = e as ServerEvents;
			doAfterDisconnect(serverArgs.ip, serverArgs.tcpSocket);
			closeSocket(serverArgs.tcpSocket);
		}

		public void FireReceivingEvent(EventArgs e)
		{
			if (null != receiving)
			{
				receiving(this, e);
			}
		}

		public void FireConnectingEvent(EventArgs e)
		{
			if (null != connecting)
			{
				connecting(this, e);
			}
		}

		public void FireDisconnectingEvent(EventArgs e)
		{
			if (null != disconnecting)
			{
				disconnecting(this, e);
			}
		}
		#endregion

		public Serwer()
		{
			receiving += dataReceived;
			connecting += newConnection;
			disconnecting += connectionClosed;
		}

		private void tcpReceiving(Socket client, int maxBufferSize = 10000)
		{
			byte[] receivedBytes = new Byte[maxBufferSize];
			try
			{
				while (client.Connected)
				{
					EndPoint ip = client.RemoteEndPoint;
					int length = client.ReceiveFrom(receivedBytes, maxBufferSize, SocketFlags.None, ref ip);
					byte[] bytes = new byte[length];
					Array.Copy(receivedBytes, bytes, length);

					ServerEvents ev = new ServerEvents((IPEndPoint)ip, bytes, client);
					FireReceivingEvent(ev);
				}
			}
			catch (SocketException)
			{
				try
				{
					ServerEvents ev = new ServerEvents((IPEndPoint)client.RemoteEndPoint, null, client);
					FireDisconnectingEvent(ev);
				}
				catch (ObjectDisposedException) { }
			}
		}

		/// <summary>Utworzenie gniazdka nasłuchującego na podanym porcie, oczekującego konkretnego protokołu.</summary>
		/// <param name="localPort">Port lokalny gniazdka</param>
		/// <param name="protocol">Protokół typu wyliczeniowego Protocol</param>
		/// <param name="maxBufferSize">Maksymalny rozmiar bufora odebranych danych</param>
		public void startServer(int localPort, Protocol protocol, int maxBufferSize = 1500)
		{
			IPEndPoint localEP = new IPEndPoint(IPAddress.Any, localPort);

			if (protocol == Protocol.UDP)
			{
				byte[] receivedBytes = new Byte[maxBufferSize];
				Socket listener = null;
				listener = new Socket(AddressFamily.InterNetwork,
					SocketType.Dgram,
					ProtocolType.Udp);
				listener.Bind(localEP);
				try
				{
					try
					{
						while (true)
						{
							EndPoint ip = new IPEndPoint(IPAddress.Any, 0);
							int length = listener.ReceiveFrom(receivedBytes, maxBufferSize, SocketFlags.None, ref ip);
							byte[] bytes = new byte[length];
							Array.Copy(receivedBytes, bytes, length);

							ServerEvents ev = new ServerEvents((IPEndPoint)ip, bytes, null);
							FireReceivingEvent(ev);
						}
					}
					finally
					{
						listener.Close();
					}
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.ToString());
				}
			}
			else if (protocol == Protocol.TCP)
			{
				TcpListener listener = new TcpListener(localEP);
				Socket client;
				listener.Start();
				try
				{
					try
					{
						while (true)
						{
							lock (_lock)
							{
								client = listener.AcceptSocket();
								ServerEvents ev = new ServerEvents((IPEndPoint)client.RemoteEndPoint, null, client);
								FireConnectingEvent(ev);

								Task task = new Task(() => tcpReceiving(client, maxBufferSize));
								task.Start();
							}
						}
					}
					finally
					{
						listener.Stop();
					}
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.ToString());
				}
			}
		}
	}
}