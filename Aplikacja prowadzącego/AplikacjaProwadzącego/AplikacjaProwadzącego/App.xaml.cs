﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using PTI_TCP;
using System.Windows.Data;
using System.ComponentModel;
using System.IO;

namespace AplikacjaProwadzącego
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		public SerwerTCP serwer;
		public List<String> blackList;

		App()
		{
			try
			{
				serwer = new SerwerTCP();
				Task task = new Task(() => serwer.startServer(12312, Serwer.Protocol.TCP, 5000000));
                task.Start();
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}

			try
			{
				blackList = new List<string>();
				String[] black = File.ReadAllLines("black.txt");
				foreach (String b in black)
				{
					blackList.Add(b);
				}
			}
			catch(Exception e)
			{
				Console.WriteLine("Wystąpił błąd podczas wczytywania z pliku " + e.Message);
			}
		}
	}
}