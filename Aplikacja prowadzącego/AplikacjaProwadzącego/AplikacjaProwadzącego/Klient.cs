﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace PTI_TCP_Klient
{
	class KlientTCP : Klient
	{
		private KlientTCP(int localPort) : base(localPort)
		{}
	}

	public class Klient
	{
		public enum Protocol {
			/// <summary>Protokół UDP</summary>
			UDP,
			/// <summary>Protokół TCP</summary>
			TCP
		}
		
		/// <summary>Port lokalny</summary>
		public int localPort { get; private set; }
		private IPEndPoint localEP;

		/// <summary>Obiekt gniazdka typu Socket</summary>
		public Socket socket { get; private set; }

		public Klient(int localPort)
		{
			this.localPort = localPort;
			localEP = new IPEndPoint(IPAddress.Any, localPort);
		}

		/// <summary>Utworzenie gniazdka nadającego na podanym protokole oraz zbindowanego do podanego portu.</summary>
		/// <param name="localPort">Port lokalny gniazdka</param>
		/// <param name="protocol">Protokół typu wyliczeniowego Protocol</param>
		public static Klient startClient(int localPort, Protocol protocol)
		{
			Klient klient = new Klient(localPort);
			Socket client = null;
			try
			{
				if (protocol == Protocol.UDP)
				{
					client = new Socket(AddressFamily.InterNetwork,
						SocketType.Dgram,
						ProtocolType.Udp);
					client.Bind(klient.localEP);
				}
				else if(protocol == Protocol.TCP)
				{
					client = new Socket(AddressFamily.InterNetwork,
						SocketType.Stream,
						ProtocolType.Tcp);
					client.Bind(klient.localEP);
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}
			klient.socket = client;
			return klient;
		}

		public void connect(string remoteIpAddress, int remotePort)
		{
			IPAddress remoteIp = IPAddress.Parse(remoteIpAddress);
			IPEndPoint remoteEP = new IPEndPoint(remoteIp, remotePort);
			try
			{
				socket.Connect(remoteEP);
			}
			catch(SocketException ex)
			{
				Console.WriteLine(ex.Message);
			}
		}

		/// <summary>Wysłanie wiadomości typu string na adres IP podany jako string.</summary>
		/// <param name="message">Wiadomość, która ma zostać wysłana</param>
		/// <param name="ip">Adres IP na który wiadomośc ma zostać wysłana</param>
		/// <param name="port">Zdalny port</param>
		public void sendMessage(string message, string ip, int port)
		{
			IPAddress remoteIpAddress = IPAddress.Parse(ip);
			IPEndPoint remoteEP = new IPEndPoint(remoteIpAddress, port);
			
			try
			{
				byte[] data;
				data = Encoding.ASCII.GetBytes(message);
				socket.SendTo(data, data.Length, SocketFlags.None, remoteEP);
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}

		/// <summary>Wysłanie tablicy bajtów na adres IP podany jako string.</summary>
		/// <param name="bytes"></param>
		/// <param name="ip">Adres IP na który wiadomośc ma zostać wysłana</param>
		/// <param name="port">Zdalny port</param>
		public void sendBytes(byte[] bytes, string ip, int port)
		{
			IPAddress remoteIpAddress = IPAddress.Parse(ip);
			IPEndPoint remoteEP = new IPEndPoint(remoteIpAddress, port);

			try
			{
				socket.SendTo(bytes, bytes.Length, SocketFlags.None, remoteEP);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}
	}
}
