﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Satelita
{
    public class Wczytywanie
    {
        private double _pamiec;
        private Collection<string> _systemProcesses = new Collection<string>();
        private List<Tranlator> _translator = new List<Tranlator>(); 
        public Wczytywanie()
        {
            try
            {
                _pamiec = 0.0;
                string line;
                System.IO.StreamReader file = new System.IO.StreamReader("SystemProcesses.txt");
                if (file != null)
                {
                    while ((line = file.ReadLine()) != null)
                    {
                        _systemProcesses.Add(line);
                    }
                }
                file.Close();
            }catch(FileNotFoundException)
            {
                Console.WriteLine("Nie można odczytać pliku SystemProcesses");
            }catch(Exception e)
            {
                Console.WriteLine(e);
            }



            try
            {
                string line;
                System.IO.StreamReader file = new System.IO.StreamReader("Lexicon.txt");
                string[] split = new string[2];
                if (file != null)
                {
                    while ((line = file.ReadLine()) != null)
                    {
                        split = line.Split('-');
                        Tranlator tranlator = new Tranlator(split[0], split[1]);
                        _translator.Add(tranlator);
                    }
                }
                file.Close();
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Nie można odczytać pliku Lexicon");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        public Process[] IloscProcesow()
        {
            
            Process[] procesy = Process.GetProcesses();

            return procesy;
        }

        public void Procesy(Stanowisko stanowisko)
        {
            Stopwatch stopwatch = new Stopwatch();

            // Begin timing.
            //stopwatch.Start();
            
            Process[] procesy = Process.GetProcesses();
			for (int i = 0; i < procesy.Length; i++)
			{
                
				_pamiec = (double)(procesy[i].WorkingSet64) / 1000000;
				_pamiec = Math.Round(_pamiec, 2);
                if(_pamiec == 0) continue;
				if (_systemProcesses.Contains(procesy[i].ProcessName.ToLower())) continue;
                string nazwa = procesy[i].ProcessName;
                //Console.WriteLine("A: {0}", stopwatch.Elapsed);
                foreach (Tranlator t in _translator)
                {
                    if (nazwa == t.pierwotne)
                        nazwa = t.przetlumaczone;
                }

                //Parallel.ForEach(_translator, t =>
                //{
                //    if (nazwa == t.pierwotne)
                //        nazwa = t.przetlumaczone;
                //});
                //Console.WriteLine("B: {0}", stopwatch.Elapsed);
                bool alreadyExists = false;
                foreach (KeyValuePair<int, Satelita.Proces> p in stanowisko.listaProcesow)
                {
                    if (p.Value.nazwa == nazwa)
                    {
                        alreadyExists = true;
                        p.Value.zuzyciePamieci += _pamiec;
                        break;
                    }
                    else alreadyExists = false;
                }
                if (alreadyExists == true) continue;

                //Console.WriteLine("C: {0}", stopwatch.Elapsed);
                Proces proces = new Proces(nazwa, _pamiec);
				stanowisko.dodajProces(procesy[i].Id, proces);
			}
            //stopwatch.Stop();

            // Write result.
            

        }
        public Bitmap ZrzutEkranu()
        {
            Bitmap screenshot = new Bitmap(SystemInformation.VirtualScreen.Width,
                               SystemInformation.VirtualScreen.Height,
                               PixelFormat.Format32bppArgb);
            Graphics screenGraph = Graphics.FromImage(screenshot);
            screenGraph.CopyFromScreen(SystemInformation.VirtualScreen.X,
                                       SystemInformation.VirtualScreen.Y,
                                       0,
                                       0,
                                       SystemInformation.VirtualScreen.Size,
                                       CopyPixelOperation.SourceCopy);
            Bitmap resized = new Bitmap(screenshot, new Size(960, 540));
            //resized.Save("DSC_0002_thumb.jpg");
            return resized;            
        }
      



    }

    public class Tranlator
    {
        public string pierwotne;
        public string przetlumaczone;

        public Tranlator (string p, string n)
        {
            pierwotne = p;
            przetlumaczone = n;
        }
    }
}