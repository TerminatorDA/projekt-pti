﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace Satelita
{
	class Program
	{
        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        const int SW_HIDE = 0;
        const int SW_SHOW = 5;
        static void Main(string[] args)
		{
            var handle = GetConsoleWindow();

            // Hide
            ShowWindow(handle, SW_HIDE);

            // Show
            //ShowWindow(handle, SW_SHOW);
            Wysylanie.startujNasluchiwanie(15001);
			//Wysylanie.startujWysylanie(12312, 1000);
			Wysylanie.aktualizujDane(900);
			Task.Run(async () =>
			{
				while (true)
				{
					await Task.Delay(9999999);
				}
			}).GetAwaiter().GetResult();
		}
	}
}
