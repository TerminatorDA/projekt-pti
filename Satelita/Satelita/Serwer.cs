﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using System.Drawing;

namespace Satelita
{
	class SerwerDaemon : Serwer
	{
		/// <summary>Lista klientow w sieci</summary>
		public Dictionary<IPEndPoint, Prowadzacy> listaKlientow { get; private set; }
		const int serverRecPort = 12312;
		private static readonly object _lock = new object();

		public SerwerDaemon() : base()
		{
			listaKlientow = new Dictionary<IPEndPoint, Prowadzacy>();
		}

		override public void doAfterReceive(IPEndPoint ip, byte[] data, Socket tcpSocket)
		{
			List<string> listaPakietow = Protokol.getListOfPackets(data);
			foreach(var pakiet in listaPakietow)
			{
				Protokol.Action akcja = Protokol.getAction(pakiet);
				string computerName = Protokol.getComputerName(pakiet);
				string userName = Protokol.getUserName(pakiet);
				
				switch (akcja)
				{
					case Protokol.Action.clientActive:
						if (!listaKlientow.ContainsKey(ip))
						{
							lock (_lock)
							{
								Prowadzacy nowe = new Prowadzacy(computerName, userName);
								listaKlientow[ip] = nowe;
								Task task = new Task(() => Wysylanie.startujWysylanie(ip, serverRecPort));
								task.Start();
							}
						}
						else
						{
							listaKlientow[ip].ustawDostepnosc(true);
						}
						break;
					case Protokol.Action.clientInactive:
						if (listaKlientow.ContainsKey(ip))
						{
							listaKlientow[ip].ustawDostepnosc(false);
							listaKlientow.Remove(ip);
						}
						break;
					case Protokol.Action.startSending:
						if (listaKlientow.ContainsKey(ip))
						{
							listaKlientow[ip].ustawWysylanie(true);
						}
						break;
					case Protokol.Action.stopSending:
						if (listaKlientow.ContainsKey(ip))
						{
							listaKlientow[ip].ustawWysylanie(false);
						}
						break;
					case Protokol.Action.killProcesses:
						// kod jak sie zabija proces
						break;
					case Protokol.Action.sendMessage:
						List<string> listaDanych = Protokol.getData(pakiet);
						foreach (var dana in listaDanych)
						{
							if (Protokol.getDataType(dana) == Protokol.DataType.message)
							{
								using(NotifyIcon notifyIcon = new NotifyIcon())
								{
									notifyIcon.Visible = true;
									notifyIcon.Icon = SystemIcons.Application;

									string[] daneWiadomosci = Protokol.getMessage(dana);
									notifyIcon.BalloonTipTitle = daneWiadomosci[1];
									notifyIcon.BalloonTipText = daneWiadomosci[2];
									notifyIcon.ShowBalloonTip(10000);
								}
							}
						}
						break;
					case Protokol.Action.byeClient:
						if (listaKlientow.ContainsKey(ip))
						{
							listaKlientow[ip].ustawDostepnosc(false);
							listaKlientow.Remove(ip);
						}
						break;
					default:
						break;
				}
				if (listaKlientow.ContainsKey(ip) && listaKlientow[ip].dostepny == false)
				{
					listaKlientow.Remove(ip);
				}
			}
		}

		public override void doAfterConnect(IPEndPoint ip, Socket tcpSocket)
		{
			throw new NotImplementedException();
		}

		public override void doAfterDisconnect(IPEndPoint ip, Socket tcpSocket)
		{
			throw new NotImplementedException();
		}
	}

	abstract class Serwer
	{
		public enum Protocol
		{
			/// <summary>Protokół UDP</summary>
			UDP,
			/// <summary>Protokół TCP</summary>
			TCP
		}

		//EVENTY
		#region
		public delegate void ServerDelegate(object sender, EventArgs e);
		public event ServerDelegate receiving;
		public event ServerDelegate connecting;
		public event ServerDelegate disconnecting;

		public class ServerEvents : EventArgs
		{
			public byte[] data { get; private set; }
			public IPEndPoint ip { get; private set; }
			public Socket tcpSocket { get; private set; }

			public ServerEvents(IPEndPoint ip, byte[] data, Socket socket)
			{
				this.ip = ip;
				this.data = data;
				this.tcpSocket = socket;
			}
		}

		abstract public void doAfterReceive(IPEndPoint ip, byte[] data, Socket tcpSocket);
		abstract public void doAfterConnect(IPEndPoint ip, Socket tcpSocket);
		abstract public void doAfterDisconnect(IPEndPoint ip, Socket tcpSocket);
		private void closeSocket(Socket tcpSocket)
		{
			if (tcpSocket.Connected)
			{
				tcpSocket.Shutdown(SocketShutdown.Both);
				tcpSocket.Close();
			}
		}
		private void dataReceived(object sender, EventArgs e)
		{
			ServerEvents serverArgs = e as ServerEvents;
			doAfterReceive(serverArgs.ip, serverArgs.data, serverArgs.tcpSocket);
		}

		private void newConnection(object sender, EventArgs e)
		{
			ServerEvents serverArgs = e as ServerEvents;
			doAfterConnect(serverArgs.ip, serverArgs.tcpSocket);
		}

		private void connectionClosed(object sender, EventArgs e)
		{
			ServerEvents serverArgs = e as ServerEvents;
			doAfterDisconnect(serverArgs.ip, serverArgs.tcpSocket);
			closeSocket(serverArgs.tcpSocket);
		}

		public void FireReceivingEvent(EventArgs e)
		{
			if (null != receiving)
			{
				receiving(this, e);
			}
		}

		public void FireConnectingEvent(EventArgs e)
		{
			if (null != connecting)
			{
				connecting(this, e);
			}
		}

		public void FireDisconnectingEvent(EventArgs e)
		{
			if (null != disconnecting)
			{
				disconnecting(this, e);
			}
		}
		#endregion

		public Serwer()
		{
			receiving += dataReceived;
			connecting += newConnection;
			disconnecting += connectionClosed;
		}

		private void tcpReceiving(Socket client, int maxBufferSize = 10000)
		{
			byte[] receivedBytes = new Byte[maxBufferSize];
			try
			{
				while (client.Connected)
				{
					EndPoint ip = client.RemoteEndPoint;
					int length = client.ReceiveFrom(receivedBytes, maxBufferSize, SocketFlags.None, ref ip);
					byte[] bytes = new byte[length];
					Array.Copy(receivedBytes, bytes, length);

					ServerEvents ev = new ServerEvents((IPEndPoint)ip, bytes, client);
					FireReceivingEvent(ev);
				}
			}
			catch (SocketException)
			{
				ServerEvents ev = new ServerEvents((IPEndPoint)client.RemoteEndPoint, null, client);
				FireDisconnectingEvent(ev);
			}
		}

		/// <summary>Utworzenie gniazdka nasłuchującego na podanym porcie, oczekującego konkretnego protokołu.</summary>
		/// <param name="localPort">Port lokalny gniazdka</param>
		/// <param name="protocol">Protokół typu wyliczeniowego Protocol</param>
		/// <param name="maxBufferSize">Maksymalny rozmiar bufora odebranych danych</param>
		public void startServer(int localPort, Protocol protocol, int maxBufferSize = 1500)
		{
			IPEndPoint localEP = new IPEndPoint(IPAddress.Any, localPort);
			
			if (protocol == Protocol.UDP)
			{
				byte[] receivedBytes = new Byte[maxBufferSize];
				Socket listener = null;
				listener = new Socket(AddressFamily.InterNetwork,
					SocketType.Dgram,
					ProtocolType.Udp);
				listener.Bind(localEP);
				try
				{
					try
					{
						while (true)
						{
							EndPoint ip = new IPEndPoint(IPAddress.Any, 0);
							int length = listener.ReceiveFrom(receivedBytes, maxBufferSize, SocketFlags.None, ref ip);
							byte[] bytes = new byte[length];
							Array.Copy(receivedBytes, bytes, length);

							ServerEvents ev = new ServerEvents((IPEndPoint)ip, bytes, null);
							FireReceivingEvent(ev);
						}
					}
					finally
					{
						listener.Shutdown(SocketShutdown.Both);
						listener.Close();
					}
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.ToString());
				}
			}
			else if (protocol == Protocol.TCP)
			{
				TcpListener listener = new TcpListener(localEP);
				Socket client;
				listener.Start();
				try
				{
					try
					{
						while (true)
						{
							client = listener.AcceptSocket();
							ServerEvents ev = new ServerEvents((IPEndPoint)client.RemoteEndPoint, null, client);
							FireConnectingEvent(ev);

							Task task = new Task(() => tcpReceiving(client, maxBufferSize));
							task.Start();
						}
					}
					finally
					{
						listener.Stop();
					}
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.ToString());
				}
			}
		}
	}
}