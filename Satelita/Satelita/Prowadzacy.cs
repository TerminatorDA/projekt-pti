﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Satelita
{
	class Prowadzacy
	{
		public string nazwaUzytkownika { get; private set; }
		public string nazwaKomputera { get; private set; }
		public DateTime ostatnioWidziany { get; private set; }
		public bool wysylanie { get; private set; }
		public bool dostepny { get; private set; }
		public bool trybSzybki { get; private set; }
		public Prowadzacy(string computer, string user)
		{
			nazwaKomputera = computer;
			nazwaUzytkownika = user;
			wysylanie = true;
			dostepny = true;
			trybSzybki = false;
			ostatnioWidziany = DateTime.Now;
		}
		public void ustawDostepnosc(bool dostepnosc)
		{
			dostepny = dostepnosc;
			ostatnioWidziany = DateTime.Now;
		}
		public void ustawWysylanie(bool wysylanie)
		{
			this.wysylanie = wysylanie;
			ostatnioWidziany = DateTime.Now;
		}
		public void ustawTrybSzybki(bool tryb)
		{
			this.trybSzybki = tryb;
			ostatnioWidziany = DateTime.Now;
		}
		public void ustawDate()
		{
			ostatnioWidziany = DateTime.Now;
		}
	}
}
