﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Satelita
{
	class Wysylanie
	{
		public static SerwerDaemon serwer { get; private set; }

		public static Wczytywanie wczytywanie = new Wczytywanie();
		
		public static Stanowisko dane;

		public static byte[] daneDoWyslania;
		
		private static readonly object _lock = new object();

		private const int firstPort = 32000;
		private const int maxPort = 32100;
		private static int lastPort = firstPort;

		private const int delayFast = 300;
		private const int delaySlow = 1000;

		private static int tasks = 0;
		
		public static void startujNasluchiwanie(int port)
		{
			serwer = new SerwerDaemon();
			Task taskSerwera = new Task(() => serwer.startServer(port, Serwer.Protocol.UDP, 15000));
			taskSerwera.Start();
		}

		public static async void aktualizujDane(int delay)
		{
			do
			{
				string computerName = Environment.MachineName;
				string userName = Environment.UserName;
				Stanowisko stanowisko = new Stanowisko(computerName, userName);

				wczytywanie.Procesy(stanowisko);
				stanowisko.aktualnyZrzutEkranu = (Image)wczytywanie.ZrzutEkranu();
				
				string screenshot = Protokol.getDataString(Protokol.DataType.screenshot, stanowisko.aktualnyZrzutEkranu);
				string listaProcesow = Protokol.getDataString(Protokol.DataType.processList, stanowisko.listaProcesow);
				StringBuilder sb = new StringBuilder();
				sb.Append(screenshot);
				sb.Append(listaProcesow);

				lock (_lock)
				{
					dane = stanowisko;
					daneDoWyslania = Protokol.newGetBytes(Protokol.Action.sendingData, stanowisko.nazwaKomputera, stanowisko.nazwaUzytkownika, sb.ToString());
				}
				//Console.WriteLine("Zaktualizowano dane: " + DateTime.Now.ToString());
				await Task.Delay(delay);
			} while (tasks > 0);
		}

		public static async void startujWysylanie(IPEndPoint ip, int port)
		{
			Klient klient = null;
			
			lock (_lock)
			{
				if (tasks == 0)
				{
					tasks = 1;
					Task task = new Task(() => aktualizujDane(900));
					task.Start();
				}
				else
					tasks++;
				klient = Klient.startClient(lastPort++, Klient.Protocol.TCP);
				if (lastPort >= maxPort || lastPort < firstPort) lastPort = firstPort;
			}
			klient.connect(ip.Address.ToString(), port);

			try
			{
				while (klient != null && klient.socket.Connected)
				{
					byte[] dane = null;
					lock (_lock)
					{
						if (!serwer.listaKlientow.ContainsKey(ip) || !serwer.listaKlientow[ip].dostepny)
							break;
						dane = daneDoWyslania;
					}
					lock (_lock)
					{
						if (serwer.listaKlientow.ContainsKey(ip) && serwer.listaKlientow[ip].wysylanie)
						{
							try
							{
								if (klient.socket.Connected)
								{
									klient.sendBytes(dane, ip.Address.ToString(), port);
									Console.WriteLine("Wysłano do: " + ip.Address.ToString() + ":" + port + " - " + DateTime.Now.ToString());
								}
							}
							catch (SocketException)
							{
								//klient.socket.Shutdown(SocketShutdown.Both);
								//klient.socket.Close();
								break;
							}
						}
					}

					if (serwer.listaKlientow.ContainsKey(ip) && serwer.listaKlientow[ip].trybSzybki)
						await Task.Delay(delayFast);
					else
						await Task.Delay(delaySlow);
				}
			}
			finally
			{
				lock (_lock)
				{
					tasks--;
				}
				if (serwer.listaKlientow.ContainsKey(ip))
					serwer.listaKlientow.Remove(ip);
				try
				{
					if (klient.socket.Connected)
					{
						string computerName = dane.nazwaKomputera;
						string userName = dane.nazwaUzytkownika;
						byte[] packet = Protokol.newGetBytes(Protokol.Action.byeServer, computerName, userName, null);

						klient.sendBytes(packet, ip);
						Console.WriteLine("Wysłano BYE do: " + ip.ToString());
					}
				}
				catch (SocketException) { }
				klient.socket.Shutdown(SocketShutdown.Both);
				klient.socket.Close();
			}
		}
	}
}
